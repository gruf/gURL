package uri

import (
	"sort"

	"codeberg.org/gruf/go-bytes"
)

// QueryParams represents parsed query parameters
type QueryParams struct {
	params []kv  // params is the slice of key-values
	buffer Bytes // buffer is the internal byte slice for building string repr
}

// NewParams returns a new initialized QueryParams object
func NewParams() QueryParams {
	return QueryParams{
		params: []kv{},
		buffer: make([]byte, 0, 64),
	}
}

// CopyTo copies the current state of QueryParams into the other object
func (q *QueryParams) CopyTo(dst *QueryParams) {
	// Reset dst parameter slice
	dst.params = dst.params[:0]

	// Allocate our cur no. params
	dst.allocParams(q.Len())

	// Copy across keys + values
	for i := range q.params {
		dst.params[i].key = append(dst.params[i].key, q.params[i].key...)
		dst.params[i].value = append(dst.params[i].value, q.params[i].value...)
	}

	// Copy across query buffer
	dst.buffer = append(dst.buffer[:0], q.buffer...)
}

// Parse parses the supplied query bytes into the QueryParams object,
// returning error on invalid percent encoding
func (q *QueryParams) Parse(b []byte) error {
	needsUnescaping, err := IsValidPercentEncoding(b)
	if err != nil {
		return err
	}
	q.parse(b, needsUnescaping)
	return nil
}

// ParseString parses the supplied query bytes into the QueryParams
// object, returning error on invalid percent encoding
func (q *QueryParams) ParseString(s string) error {
	return q.Parse(bytes.StringToBytes(s))
}

func (q *QueryParams) parse(b []byte, needsUnescaping bool) {
	q.Reset()

	// Ensure at least one param alloc'd
	q.allocParams(1)
	cur := q.curParam() // ptr to cur param

	// Iterate through bytes
	last := 0
	isKey := true
	for i := 0; i < len(b); i++ {
		switch b[i] {
		case '=':
			if isKey {
				// Check if it needs escaping, else
				// just set the bytes directly
				if needsUnescaping {
					queryUnescapeNoCheck(&q.buffer, b[last:i])
					cur.key = append(cur.key[:0], q.buffer...)
					q.buffer = q.buffer[:0]
				} else {
					cur.key = append(cur.key[:0], b[last:i]...)
				}

				// Set last pos
				last = i + 1

				// Toggle
				isKey = false
			}
		case '&':
			if isKey {
				// Check if it needs escaping, else
				// just set the bytes directly
				if needsUnescaping {
					queryUnescapeNoCheck(&q.buffer, b[last:i])
					cur.key = append(cur.key[:0], q.buffer...)
					q.buffer = q.buffer[:0]
				} else {
					cur.key = append(cur.key[:0], b[last:i]...)
				}

				// Reset value
				cur.value = cur.value[:0]
			} else {
				// Check if it needs escaping, else
				// just set the bytes directly
				if needsUnescaping {
					queryUnescapeNoCheck(&q.buffer, b[last:i])
					cur.value = append(cur.value[:0], q.buffer...)
					q.buffer = q.buffer[:0]
				} else {
					cur.value = append(cur.value[:0], b[last:i]...)
				}

				// Toggle
				isKey = true
			}

			// Set last pos
			last = i + 1

			// Only allocate next if not last
			if i < len(b)-1 {
				q.allocParams(1)
				cur = q.curParam()
			}
		}
	}

	// The final parameter's probably not "&"" terminated
	if last < len(b) {
		if isKey {
			// Check if it needs escaping, else
			// just set the bytes directly
			if needsUnescaping {
				queryUnescapeNoCheck(&q.buffer, b[last:])
				cur.key = append(cur.key[:0], q.buffer...)
				q.buffer = q.buffer[:0]
			} else {
				cur.key = append(cur.key[:0], b[last:]...)
			}

			// Reset value
			cur.value = cur.value[:0]
		} else {
			// Check if it needs escaping, else
			// just set the bytes directly
			if needsUnescaping {
				queryUnescapeNoCheck(&q.buffer, b[last:])
				cur.value = append(cur.value[:0], q.buffer...)
				q.buffer = q.buffer[:0]
			} else {
				cur.value = append(cur.value[:0], b[last:]...)
			}
		}
	}
}

// Len returns the number of query key-value pairs
func (q *QueryParams) Len() int {
	return len(q.params)
}

// Params returns the the current state of the QueryParams object as bytes.Bytes
func (q *QueryParams) Params() bytes.Bytes {
	// Reset internal buf
	q.buffer = q.buffer[:0]

	for i := 0; i < len(q.params); i++ {
		// Append key to buf
		q.buffer = append(q.buffer, q.params[i].key...)

		// Append '=' and value if found
		if len(q.params[i].value) > 0 {
			q.buffer = append(q.buffer, '=')
			q.buffer = append(q.buffer, q.params[i].value...)
		}

		// Append '&' separator
		q.buffer = append(q.buffer, '&')
	}

	// Drop final separator
	if q.buffer.Len() > 0 {
		q.buffer = q.buffer[:q.buffer.Len()-1]
	}

	// Return buffer
	return &q.buffer
}

// Add adds the supplied value to the parameter at supplied key,
// using the supplied 'join' as a separator byte. A new parameter
// will be created if necessary
func (q *QueryParams) Add(key []byte, value []byte, join byte) {
	param, ok := q.paramWithKey(key)
	if !ok {
		// Allocate new param
		q.allocParams(1)
		param = q.curParam()

		// Simply set key, value and val flag
		param.key = append(param.key[:0], key...)
		param.value = append(param.value[:0], value...)
	} else {
		// Only try add if non-zero
		if len(value) > 0 {
			if len(param.value) > 0 {
				// Append separator
				param.value = append(param.value, join)
			}

			// Add the value
			param.value = append(param.value, value...)
		}
	}
}

// AddString adds the supplied value to the parameter at supplied key,
// using the supplied 'join' as a separator byte. A new parameter
// will be created if necessary
func (q *QueryParams) AddString(key string, value string, join byte) {
	q.Add(bytes.StringToBytes(key), bytes.StringToBytes(value), join)
}

// Get attempts to return the param value for supplied key
func (q *QueryParams) Get(key []byte) (Bytes, bool) {
	param, ok := q.paramWithKey(key)
	if !ok {
		return nil, false
	}
	return param.value, true
}

// GetString attempts to return the param value for supplied key
func (q *QueryParams) GetString(key string) (Bytes, bool) {
	return q.Get(bytes.StringToBytes(key))
}

// Set sets the parameter with key, to the supplied value. A new
// parameter will be created if necessary
func (q *QueryParams) Set(key []byte, value []byte) {
	param, ok := q.paramWithKey(key)
	if !ok {
		// Allocate new param
		q.allocParams(1)
		param = q.curParam()
	}

	// Set the paramater value
	param.value = append(param.value[:0], value...)
}

// SetString sets the parameter with key, to the supplied value.
// A new parameter will be created if necessary
func (q *QueryParams) SetString(key string, value string) {
	q.Set(bytes.StringToBytes(key), bytes.StringToBytes(value))
}

// Delete deletes the paramater with key from the QueryParams object
func (q *QueryParams) Delete(key []byte) {
	for i := 0; i < len(q.params); i++ {
		if bytes.Equal(key, q.params[i].key) {
			q.params = append(q.params[:i], q.params[i+1:]...)
			return
		}
	}
}

// DeleteString deletes the paramater with key from the QueryParams object
func (q *QueryParams) DeleteString(key string) {
	q.Delete(bytes.StringToBytes(key))
}

// Sort sorts the query parameters alphabetically by key
func (q *QueryParams) Sort() {
	sort.Slice(q.params, func(i, j int) bool {
		return bytes.BytesToString(q.params[i].key) <= bytes.BytesToString(q.params[j].key)
	})
}

// Reset resets the QueryParams object
func (q *QueryParams) Reset() {
	q.params = q.params[:0]
	q.buffer = q.buffer[:0]
}

// paramWithKey attempts to fetch the key-value object for supplied key
func (q *QueryParams) paramWithKey(key []byte) (*kv, bool) {
	for i := range q.params {
		if bytes.Equal(key, q.params[i].key) {
			return &q.params[i], true
		}
	}
	return nil, false
}

// curParam returns a ptr to param at current index
func (q *QueryParams) curParam() *kv {
	return &q.params[len(q.params)-1]
}

// allocParams ensures that at least 'count' params are allocated
func (q *QueryParams) allocParams(count int) {
	l := q.Len()
	if l+count < cap(q.params) {
		// We have room, we can reslice
		q.params = q.params[:l+count]
	} else {
		upTo := l + count

		// Reallocate slice with more space
		params := make([]kv, cap(q.params)+2*count)
		copy(params, q.params)
		q.params = params[:upTo]

		// Allocate each of new kvs
		for i := l; i < upTo; i++ {
			q.params[i] = kv{
				key:   make([]byte, 0, 32),
				value: make([]byte, 0, 32),
			}
		}
	}
}

type kv struct {
	key   Bytes
	value Bytes
}
