package uri

import "sync"

var (
	_uriPool sync.Pool
	uriOnce  = sync.Once{}

	_paramPool sync.Pool
	paramOnce  = sync.Once{}
)

// uriPool is the global URI object pool
func uriPool() *sync.Pool {
	uriOnce.Do(func() {
		_uriPool = sync.Pool{
			New: func() interface{} {
				uri := New()
				return &uri
			},
		}
	})
	return &_uriPool
}

// paramPool is the global QueryParams object pool
func paramPool() *sync.Pool {
	paramOnce.Do(func() {
		_paramPool = sync.Pool{
			New: func() interface{} {
				params := NewParams()
				return &params
			},
		}
	})
	return &_paramPool
}

// AcquireURI acquires a URI object from the global pool
func AcquireURI() *URI {
	return uriPool().Get().(*URI)
}

// ReleaseURI resets and releases a URI object back to the pool
func ReleaseURI(uri *URI) {
	uri.Reset()
	uriPool().Put(uri)
}

// AcquireQueryParams acquires a QueryParams object from the global pool
func AcquireQueryParams() *QueryParams {
	return paramPool().Get().(*QueryParams)
}

// ReleaseQueryParams resets and releases a QueryParams object back to the pool
func ReleaseQueryParams(params *QueryParams) {
	params.Reset()
	paramPool().Put(params)
}
