package ctx

import (
	"context"
	"sync"
	"time"
)

// Context is a reusable context.Context implementation, intended to be
// used as a singular context passed down through a data structure.
type Context struct {
	data data          // context key-value store
	done chan struct{} // done / cancel propagation
	to   *time.Timer   // timeout/deadline handler
	dl   time.Time     // deadline time
	err  error         // error set on cancel/timeout
	mu   sync.Mutex
}

// New returns a newly instantiated Context.
func New() *Context {
	return &Context{
		done: make(chan struct{}),
	}
}

// Deadline implements context.Context .Deadline().
func (ctx *Context) Deadline() (deadline time.Time, ok bool) {
	ctx.mu.Lock()
	dl := ctx.dl
	ctx.mu.Unlock()
	return dl, !dl.IsZero()
}

// WithTimeout updates the context deadline to now+timeout, time <= Now() does nothing.
func (ctx *Context) WithTimeout(timeout time.Duration) {
	ctx.WithDeadline(time.Now().Add(timeout))
}

// WithDeadline updates the context deadline to supplied time, time <= Now() does nothing.
func (ctx *Context) WithDeadline(deadline time.Time) {
	ctx.mu.Lock()

	// Check not cancelled
	if ctx.err != nil {
		ctx.mu.Unlock()
		return
	}

	// Only do anything if we have a valid timeout
	if timeout := time.Until(deadline); timeout > 0 {
		if ctx.to == nil {
			// New timer needs allocating
			ctx.to = time.NewTimer(timeout)
		} else {
			// Cancel previous set deadline
			if !ctx.dl.IsZero() &&
				!ctx.to.Stop() {
				<-ctx.to.C
			}

			// Reset the current timer
			ctx.to.Reset(timeout)
		}

		// Wait on timeout and current 'done'
		go func(done chan struct{}) {
			select {
			case <-done:
			case <-ctx.to.C:
				ctx.mu.Lock()
				ctx.err = context.DeadlineExceeded
				close(done)
				ctx.mu.Unlock()
			}
		}(ctx.done)

		// Finally set deadline
		ctx.dl = deadline
	}

	ctx.mu.Unlock()
}

// Done implements context.Context .Done().
func (ctx *Context) Done() <-chan struct{} {
	ctx.mu.Lock()
	ch := ctx.done
	ctx.mu.Unlock()
	return ch
}

// WithCancel returns a cancel function for this context.
func (ctx *Context) WithCancel() func() {
	// Get pointer to current done channel,
	// as we don't want this function to be
	// valid for all future use of this ctx
	ctx.mu.Lock()
	done := ctx.done
	ctx.mu.Unlock()

	// Return cancel function
	return func() {
		select {
		case <-done:
		default:
			ctx.mu.Lock()
			cancelTimer(ctx.to) // don't hit deadline
			ctx.err = context.Canceled
			close(done)
			ctx.mu.Unlock()
		}
	}
}

// Err implements context.Context .Err().
func (ctx *Context) Err() error {
	ctx.mu.Lock()
	err := ctx.err
	ctx.mu.Unlock()
	return err
}

// Value implements context.Context .Value().
func (ctx *Context) Value(key interface{}) interface{} {
	ctx.mu.Lock()
	v := ctx.data.Get(key)
	ctx.mu.Unlock()
	return v
}

// WithValue sets the supplied key-value pair in context data.
func (ctx *Context) WithValue(key interface{}, value interface{}) {
	ctx.mu.Lock()
	ctx.data.Set(key, value)
	ctx.mu.Unlock()
}

// Reset will cancel and reset the given context.
func (ctx *Context) Reset() {
	ctx.mu.Lock()
	cancelTimer(ctx.to)
	ctx.done = make(chan struct{})
	ctx.data.Reset()
	ctx.dl = time.Time{}
	ctx.err = nil
	ctx.mu.Unlock()
}

// cancelTimer simply cancels and drains a timer.
func cancelTimer(t *time.Timer) {
	if t != nil && !t.Stop() {
		select {
		case <-t.C:
		default:
		}
	}
}
