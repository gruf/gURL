package fsys

import (
	"runtime"

	"golang.org/x/sys/unix"
)

// to_internal_fd converts a raw file descriptor to our baseFD representation.
func to_internal_fd(fd int) int64 {
	return int64(fd) + 1
}

// baseFD provides a base set of helper methods for a file descriptor.
type baseFD struct{ fd int64 }

// Close will close the file desciptor (noop if already closed).
func (fd *baseFD) Close() error {
	if fd.fd == 0 {
		return nil
	}

	defer func() {
		// Mark closed
		fd.fd = 0

		// No need for close on dealloc
		runtime.SetFinalizer(fd, nil)
	}()

	return ignoreEINTR(func() error {
		return unix.Close(fd.FD())
	})
}

// FD returns the underlying file descriptor.
func (fd *baseFD) FD() int {
	return int(fd.fd - 1)
}
