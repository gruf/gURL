package fsys

import (
	"runtime"
	"unsafe"

	"golang.org/x/sys/unix"
)

// Dirent is a type alias to unix.Dirent.
type Dirent unix.Dirent

// IsCurDir returns whether Dirent represents ".".
func (d *Dirent) IsCurDir() bool {
	return d.Name[0] == '.' &&
		d.Name[1] == 0
}

// IsPrevDir returns whether Dirent represents "..".
func (d *Dirent) IsPrevDir() bool {
	return d.Name[0] == '.' &&
		d.Name[1] == '.' &&
		d.Name[2] == 0
}

// Name_ returns the Dirent name as usable Go string.
func (d *Dirent) Name_() string {
	var i int

	// Get string NUL terminator.
	for ; i < len(d.Name); i++ {
		if d.Name[i] == 0 {
			break
		}
	}

	// Empty str.
	if i == 0 {
		return ""
	}

	// Get actual name slice.
	name := d.Name[:i]

	// Cast []int8 name to string-able []uint8.
	bname := *(*[]byte)(unsafe.Pointer(&name))

	// Return string copy.
	return string(bname)
}

// Dir represents a (directory) file descriptor, providing syscall helper methods.
// Note that it skips a lot of the nuances offered by os.File{} for other file types,
// and provides no form of protection for concurrent use.
type Dir struct{ baseFile }

// OpenDir will open file descriptor at path with mode and permissions (must be directory).
func OpenDir(path string, mode int, perm uint32) (dir *Dir, stat Stat, err error) {
	// Open file descriptor at path with opts.
	base, stat, err := open(path, mode, perm)
	if err != nil {
		return nil, stat, err
	}

	// Check this is a directory.
	if stat.Mode&unix.S_IFDIR == 0 {
		_ = base.Close()
		return nil, stat, ErrUnsupported
	}

	// Return wrapped file descriptor.
	return newDir(base), stat, err
}

// NewDir returns a new Dir instance from given file descriptor and path.
func NewDir(fd int, filepath string) *Dir {
	return newDir(newBaseFile(fd, filepath))
}

// newDir wraps a baseFD in Dir and adds a runtime finalizer.
func newDir(file baseFile) *Dir {
	d := &Dir{file}

	// Ensure file gets closed on dealloc.
	runtime.SetFinalizer(d, func(d *Dir) {
		_ = d.Close()
	})

	return d
}

// ReadDirents calls getdents() on file descriptor, using given buffer.
func (d *Dir) ReadDirents(buf []byte) (ents []Dirent, err error) {
	if d.fd == 0 {
		return nil, ErrAlreadyClosed
	}

	if buf == nil {
		// alloc default buf size.
		buf = make([]byte, 4096)
	}

	for {
		var nn int

		// Read next block of directory entries.
		if err := ignoreEINTR(func() error {
			nn, err = unix.Getdents(d.FD(), buf)
			return err
		}); err != nil {
			return nil, err
		}

		if nn == 0 {
			// Reached end.
			break
		}

	iter:
		for off := 0; off < nn; {
			// Directly cast bytes at offset to Dirent{}.
			ent := (*Dirent)(unsafe.Pointer(&buf[off]))

			// Increase offset by length.
			off += int(ent.Reclen)

			// Skip the '.' and '..' dir entries.
			if ent.IsCurDir() || ent.IsPrevDir() {
				continue iter
			}

			// Append entry to return slice.
			ents = append(ents, *ent)
		}
	}

	return ents, nil
}

// IterDirents calls getdents() on file descriptor, using given buffer. Entries are passed to fn.
func (d *Dir) IterDirents(buf []byte, fn func(*Dirent) error) (err error) {
	if d.fd == 0 {
		return ErrAlreadyClosed
	}

	if buf == nil {
		// alloc default buf size.
		buf = make([]byte, 4096)
	}

	for {
		var nn int

		// Read next block of directory entries.
		if err := ignoreEINTR(func() error {
			nn, err = unix.Getdents(d.FD(), buf)
			return err
		}); err != nil {
			return err
		}

		if nn == 0 {
			// Reached end.
			break
		}

		for off := 0; off < nn; {
			// Directly cast bytes at offset to Dirent{}.
			ent := (*Dirent)(unsafe.Pointer(&buf[off]))

			// Pass dir entry to given handler.
			if err := fn(ent); err != nil {
				if err == StopIterator {
					err = nil
				}
				return err
			}

			// Increase offset by length.
			off += int(ent.Reclen)
		}
	}

	return nil
}
