package fsys

import (
	"io"

	"golang.org/x/sys/unix"
)

func syscall_read(fd int, buf []byte) (nn int, err error) {
	for {
		// Make read syscall to F.D
		err = ignoreEINTR(func() error {
			nn, err = unix.Read(fd, buf)
			return err
		})

		if err != nil {
			nn = 0 // failed read
			return nn, err
		}

		if nn == 0 {
			// Reached end of input
			err = io.EOF
		}

		return nn, err
	}
}

func syscall_write(fd int, buf []byte) (nn int, err error) {
	for {
		var n int

		// Make write syscall to F.D
		err = ignoreEINTR(func() error {
			n, err = unix.Write(fd, buf[nn:])
			return err
		})

		if n > 0 {
			// Update total
			nn += n
		}

		if nn == len(buf) {
			// Reached end
			return nn, err
		}

		if err != nil {
			// Write error
			return nn, err
		}

		if n == 0 {
			// Zero-length write without err
			return nn, io.ErrUnexpectedEOF
		}
	}
}

// ignoreEINTR will attempt call until error returned is *not* unix.EINTR.
func ignoreEINTR(call func() error) error {
	for {
		err := call()
		if err == unix.EINTR {
			continue // retry on EINTR
		}
		return err
	}
}
