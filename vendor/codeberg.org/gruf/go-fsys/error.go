package fsys

import (
	"errors"

	"golang.org/x/sys/unix"
)

var (
	// ErrUnsupported is returned on attempt to open a directory as File{}, or non-directory as Dir{}.
	ErrUnsupported = errors.New("unsupported file mode")

	// ErrAlreadyClosed is returned on use of a closed (also, I guess, unopened) file.
	ErrAlreadyClosed = errors.New("use of closed file")

	// StopIterator is a flag error used to signify early return in iterator functions. It will never be returned.
	StopIterator = errors.New("[BUG]: ErrStopIter should never be returned")
)

func IsNotFoundErr(err error) bool {
	return err == unix.ENOENT
}

func IsExistErr(err error) bool {
	return err == unix.EEXIST ||
		err == unix.ENOTEMPTY
}

func IsPermissionErr(err error) bool {
	return err == unix.EACCES ||
		err == unix.EPERM
}
