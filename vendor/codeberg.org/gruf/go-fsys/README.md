# go-fsys

fsys provides an alternative means of accessing regular (for now...) files on a unix filesystem.

it is easier than using `syscall.___()`, but much simpler than `os.File{}` in implementation. the latter where possible provides I/O over the netpoll system built-in to the runtime, which is a little overkill if you're just writing a little utility binary. it also provides protection for concurrent access, which again is a little overkill in some situations.

don't use this unless you know what you're doing, or unless you're stupid / reckless and willing to ride life by the seat of your jorts like me.