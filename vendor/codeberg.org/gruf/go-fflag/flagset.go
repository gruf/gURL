package fflag

import (
	"fmt"
	"strings"
	"time"
	"unsafe"

	"codeberg.org/gruf/go-bytesize"
	"codeberg.org/gruf/go-byteutil"
	"golang.org/x/exp/slices"
)

type FlagSet struct {
	flags []*Flag
	funcs []func()
}

// GetShort will fetch the flag with matching short name.
func (set *FlagSet) GetShort(name string) *Flag {
	for _, flag := range set.flags {
		if flag.Short == name {
			return flag
		}
	}
	return nil
}

// GetLong will fetch the flag with matching long name.
func (set *FlagSet) GetLong(name string) *Flag {
	for _, flag := range set.flags {
		if flag.Long == name {
			return flag
		}
	}
	return nil
}

// Sort will sort the internal slice of flags, first by short name, then long name.
func (set *FlagSet) Sort() {
	slices.SortFunc(set.flags, func(i, j *Flag) bool {
		switch {
		case i.Short != "":
			return (i.Short < j.Short) || (j.Short == "")

		case i.Long != "":
			return (j.Short == "") && (i.Long < j.Long)

		default:
			return false
		}
	})
}

// Usage returns a string showing typical terminal-formatted usage information for all of the registered flags.
func (set *FlagSet) Usage() string {
	// Ensure sorted
	set.Sort()

	// Allocate string buffer
	l := len(set.flags) * 32
	b := make([]byte, 0, l)
	buf := byteutil.Buffer{B: b}

	for i := range set.flags {
		// Append each flag's usage information
		buf.B = set.flags[i].AppendUsage(buf.B)
	}

	return buf.String()
}

func (set *FlagSet) Func(short string, long string, usage string, fn func()) {
	ptr := set.Bool(short, long, false, usage)
	set.funcs = append(set.funcs, func() {
		if *ptr {
			fn()
		}
	})
}

func (set *FlagSet) Bool(short string, long string, value bool, usage string) *bool {
	ptr := new(bool)
	set.BoolVar(ptr, short, long, value, usage)
	return ptr
}

func (set *FlagSet) BoolVar(ptr *bool, short string, long string, value bool, usage string) {
	var vstr string
	if value {
		vstr = (*boolVar)(&value).String()
	}
	set.Var((*boolVar)(ptr), short, long, vstr, usage)
}

func (set *FlagSet) Int(short string, long string, value int, usage string) *int {
	ptr := new(int)
	set.IntVar(ptr, short, long, value, usage)
	return ptr
}

func (set *FlagSet) IntVar(ptr *int, short string, long string, value int, usage string) {
	var vstr string
	if value != 0 {
		vstr = (*intVar)(&value).String()
	}
	set.Var((*intVar)(ptr), short, long, vstr, usage)
}

func (set *FlagSet) Int64(short string, long string, value int64, usage string) *int64 {
	ptr := new(int64)
	set.Int64Var(ptr, short, long, value, usage)
	return ptr
}

func (set *FlagSet) Int64Var(ptr *int64, short string, long string, value int64, usage string) {
	var vstr string
	if value != 0 {
		vstr = (*int64Var)(&value).String()
	}
	set.Var((*int64Var)(ptr), short, long, vstr, usage)
}

func (set *FlagSet) Uint(short string, long string, value uint, usage string) *uint {
	ptr := new(uint)
	set.UintVar(ptr, short, long, value, usage)
	return ptr
}

func (set *FlagSet) UintVar(ptr *uint, short string, long string, value uint, usage string) {
	var vstr string
	if value != 0 {
		vstr = (*uintVar)(&value).String()
	}
	set.Var((*uintVar)(ptr), short, long, vstr, usage)
}

func (set *FlagSet) Uint64(short string, long string, value uint64, usage string) *uint64 {
	ptr := new(uint64)
	set.Uint64Var(ptr, short, long, value, usage)
	return ptr
}

func (set *FlagSet) Uint64Var(ptr *uint64, short string, long string, value uint64, usage string) {
	var vstr string
	if value != 0 {
		vstr = (*uint64Var)(&value).String()
	}
	set.Var((*uint64Var)(ptr), short, long, vstr, usage)
}

func (set *FlagSet) Float(short string, long string, value float64, usage string) *float64 {
	ptr := new(float64)
	set.FloatVar(ptr, short, long, value, usage)
	return ptr
}

func (set *FlagSet) FloatVar(ptr *float64, short string, long string, value float64, usage string) {
	var vstr string
	if value != 0 {
		vstr = (*float64Var)(&value).String()
	}
	set.Var((*float64Var)(ptr), short, long, vstr, usage)
}

func (set *FlagSet) String(short string, long string, value string, usage string) *string {
	ptr := new(string)
	set.StringVar(ptr, short, long, value, usage)
	return ptr
}

func (set *FlagSet) StringVar(ptr *string, short string, long string, value string, usage string) {
	set.Var((*stringVar)(ptr), short, long, value, usage)
}

func (set *FlagSet) Size(short string, long string, value bytesize.Size, usage string) *bytesize.Size {
	ptr := new(bytesize.Size)
	set.SizeVar(ptr, short, long, value, usage)
	return ptr
}

func (set *FlagSet) SizeVar(ptr *bytesize.Size, short string, long string, value bytesize.Size, usage string) {
	var vstr string
	if value != 0 {
		vstr = value.StringIEC()
	}
	set.Var((*sizeVar)(ptr), short, long, vstr, usage)
}

func (set *FlagSet) Duration(short string, long string, value time.Duration, usage string) *time.Duration {
	ptr := new(time.Duration)
	set.DurationVar(ptr, short, long, value, usage)
	return ptr
}

func (set *FlagSet) DurationVar(ptr *time.Duration, short string, long string, value time.Duration, usage string) {
	var vstr string
	if value != 0 {
		vstr = value.String()
	}
	set.Var((*durationVar)(ptr), short, long, vstr, usage)
}

func (set *FlagSet) Bytes(short string, long string, value []byte, usage string) *[]byte {
	ptr := new([]byte)
	set.BytesVar(ptr, short, long, value, usage)
	return ptr
}

func (set *FlagSet) BytesVar(ptr *[]byte, short string, long string, value []byte, usage string) {
	set.Var((*bytesVar)(ptr), short, long, *(*string)(unsafe.Pointer(&value)), usage)
}

func (set *FlagSet) BoolSlice(short string, long string, value []bool, usage string) *[]bool {
	ptr := new([]bool)
	set.BoolSliceVar(ptr, short, long, value, usage)
	return ptr
}

func (set *FlagSet) BoolSliceVar(ptr *[]bool, short string, long string, value []bool, usage string) {
	var vstr string
	if value != nil {
		vstr = (*boolSliceVar)(&value).String()
	}
	set.Var((*boolSliceVar)(ptr), short, long, vstr, usage)
}

func (set *FlagSet) IntSlice(short string, long string, value []int, usage string) *[]int {
	ptr := new([]int)
	set.IntSliceVar(ptr, short, long, value, usage)
	return ptr
}

func (set *FlagSet) IntSliceVar(ptr *[]int, short string, long string, value []int, usage string) {
	var vstr string
	if value != nil {
		vstr = (*intSliceVar)(&value).String()
	}
	set.Var((*intSliceVar)(ptr), short, long, vstr, usage)
}

func (set *FlagSet) Int64Slice(short string, long string, value []int64, usage string) *[]int64 {
	ptr := new([]int64)
	set.Int64SliceVar(ptr, short, long, value, usage)
	return ptr
}

func (set *FlagSet) Int64SliceVar(ptr *[]int64, short string, long string, value []int64, usage string) {
	var vstr string
	if value != nil {
		vstr = (*int64SliceVar)(&value).String()
	}
	set.Var((*int64SliceVar)(ptr), short, long, vstr, usage)
}

func (set *FlagSet) UintSlice(short string, long string, value []uint, usage string) *[]uint {
	ptr := new([]uint)
	set.UintSliceVar(ptr, short, long, value, usage)
	return ptr
}

func (set *FlagSet) UintSliceVar(ptr *[]uint, short string, long string, value []uint, usage string) {
	var vstr string
	if value != nil {
		vstr = (*uintSliceVar)(&value).String()
	}
	set.Var((*uintSliceVar)(ptr), short, long, vstr, usage)
}

func (set *FlagSet) Uint64Slice(short string, long string, value []uint64, usage string) *[]uint64 {
	ptr := new([]uint64)
	set.Uint64SliceVar(ptr, short, long, value, usage)
	return ptr
}

func (set *FlagSet) Uint64SliceVar(ptr *[]uint64, short string, long string, value []uint64, usage string) {
	var vstr string
	if value != nil {
		vstr = (*uint64SliceVar)(&value).String()
	}
	set.Var((*uint64SliceVar)(ptr), short, long, vstr, usage)
}

func (set *FlagSet) FloatSlice(short string, long string, value []float64, usage string) *[]float64 {
	ptr := new([]float64)
	set.FloatSliceVar(ptr, short, long, value, usage)
	return ptr
}

func (set *FlagSet) FloatSliceVar(ptr *[]float64, short string, long string, value []float64, usage string) {
	var vstr string
	if value != nil {
		vstr = (*float64SliceVar)(&value).String()
	}
	set.Var((*float64SliceVar)(ptr), short, long, vstr, usage)
}

func (set *FlagSet) StringSlice(short string, long string, value []string, usage string) *[]string {
	ptr := new([]string)
	set.StringSliceVar(ptr, short, long, value, usage)
	return ptr
}

func (set *FlagSet) StringSliceVar(ptr *[]string, short string, long string, value []string, usage string) {
	var vstr string
	if value != nil {
		vstr = (*stringSliceVar)(&value).String()
	}
	set.Var((*stringSliceVar)(ptr), short, long, vstr, usage)
}

func (set *FlagSet) SizeSlice(short string, long string, value []bytesize.Size, usage string) *[]bytesize.Size {
	ptr := new([]bytesize.Size)
	set.SizeSliceVar(ptr, short, long, value, usage)
	return ptr
}

func (set *FlagSet) SizeSliceVar(ptr *[]bytesize.Size, short string, long string, value []bytesize.Size, usage string) {
	var vstr string
	if value != nil {
		vstr = (*sizeSliceVar)(&value).String()
	}
	set.Var((*sizeSliceVar)(ptr), short, long, vstr, usage)
}

func (set *FlagSet) DurationSlice(short string, long string, value []time.Duration, usage string) *[]time.Duration {
	ptr := new([]time.Duration)
	set.DurationSliceVar(ptr, short, long, value, usage)
	return ptr
}

func (set *FlagSet) DurationSliceVar(ptr *[]time.Duration, short string, long string, value []time.Duration, usage string) {
	var vstr string
	if value != nil {
		vstr = (*durationSliceVar)(ptr).String()
	}
	set.Var((*durationSliceVar)(ptr), short, long, vstr, usage)
}

// Var will add given Value implementation to the Flagset, with given short / long names and usage, and encoded default 'value' string.
func (set *FlagSet) Var(ptr Value, short string, long string, value string, usage string) {
	set.Add(Flag{
		Short:    short,
		Long:     long,
		Usage:    usage,
		Required: false,
		Default:  value,
		Value:    ptr,
	})
}

// Add will check the validity of, and add given flag to this FlagSet.
func (set *FlagSet) Add(flag Flag) {
	switch {
	// Flag contains no matchable name
	case flag.Short == "" && flag.Long == "":
		panic("no flag short or long name")

	// Flag short name must be a single char
	case len(flag.Short) > 1:
		panic("flag short name must be single char")

	// Flag names cannot start with '-'
	case strings.HasPrefix(flag.Short, "-"):
		panic("flag short name starts with hypen")
	case strings.HasPrefix(flag.Long, "-"):
		panic("flag long name starts with hypen")

	// Flag names must not contain any illegal chars
	case strings.ContainsAny(flag.Short, illegalChars):
		panic("flag short name contains illegal char")
	case strings.ContainsAny(flag.Long, illegalChars):
		panic("flag long name contains illegal char(s)")

	// Flag names must be unique
	case set.GetShort(flag.Short) != nil:
		panic("flag short name conflict")
	case set.GetLong(flag.Long) != nil:
		panic("flag long name conflict")

	// Check for flag dst
	case flag.Value == nil:
		panic("nil flag value destination")
	}

	// Append flag to internal slice
	set.flags = append(set.flags, &flag)
}

func (set *FlagSet) Parse(args []string) ([]string, error) {
	var unused []string

	for i := 0; i < len(args); i++ {
		var (
			// Current argument
			arg = args[i]

			// Matching flag for arg
			flag *Flag

			// Current flag's matching string value (if set)
			value *string
		)

		if len(arg) == 0 || len(arg) == 1 || arg[0] != '-' {
			// Unrecognizable argument, add to unused
			unused = append(unused, arg)
			continue
		}

		// Check if this has compound value (e.g. --verbose=true)
		if idx := strings.IndexByte(arg, '='); idx > 0 {
			v := arg[idx+1:]
			value = &v
			arg = arg[:idx]
		}

		if arg[1] == '-' {
			// Prefix='--' --> parse long flag
			flag = set.GetLong(arg[2:])
		} else {
			// Prefix='-' --> parse short flag
			flag = set.GetShort(arg[1:])
		}

		if flag == nil {
			// Unrecognized argument
			unused = append(unused, arg)
			continue
		}

		if value == nil {
			if _, ok := flag.Value.(*boolVar); ok {
				// Only booleans allow no value
				flag.RawValues = []string{"true"}
				continue
			}

			if i == len(args)-1 {
				// Value expected and none was found
				return nil, fmt.Errorf("flag %q expects a value", arg)
			}

			// Grab next as value
			value = &args[i+1]
			i++
		}

		// Append next arg value to existing values slice
		flag.RawValues = append(flag.RawValues, *value)
	}

loop:
	for i := range set.flags {
		// Get flag at position
		flag := set.flags[i]

		switch {
		// Values were provided for flag
		case flag.RawValues != nil:

		// Flag is required but no values
		case flag.Required:
			return nil, fmt.Errorf("flag %q is required", set.flags[i].Name())

		// Flag is unset but has default
		case flag.Default != "":
			flag.RawValues = []string{set.flags[i].Default}

		// Unset flag, ignore
		default:
			continue loop
		}

		for _, value := range flag.RawValues {
			// Attempt to parse flag value from raw values
			if err := flag.Value.Set(value); err != nil {
				return nil, fmt.Errorf("error parsing %q value: %w", flag.Name(), err)
			}
		}
	}

	// Call any set functions
	for _, fn := range set.funcs {
		fn()
	}

	return unused, nil
}

// MustParse will call FlagSet.Parse(args), panicking on error, and returning any unused arguments.
func (set *FlagSet) MustParse(args []string) []string {
	unused, err := set.Parse(args)
	if err != nil {
		panic(err)
	}
	return unused
}
