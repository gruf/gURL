package fflag

import (
	"os"
	"time"

	"codeberg.org/gruf/go-bytesize"
)

var global FlagSet

func GetShort(name string) *Flag {
	return global.GetShort(name)
}

func GetLong(name string) *Flag {
	return global.GetLong(name)
}

func Usage() string {
	return global.Usage()
}

func Func(short string, long string, usage string, fn func()) {
	global.Func(short, long, usage, fn)
}

func Bool(short string, long string, value bool, usage string) *bool {
	return global.Bool(short, long, value, usage)
}

func BoolVar(ptr *bool, short string, long string, value bool, usage string) {
	global.BoolVar(ptr, short, long, value, usage)
}

func Int(short string, long string, value int, usage string) *int {
	return global.Int(short, long, value, usage)
}

func IntVar(ptr *int, short string, long string, value int, usage string) {
	global.IntVar(ptr, short, long, value, usage)
}

func Int64(short string, long string, value int64, usage string) *int64 {
	return global.Int64(short, long, value, usage)
}

func Int64Var(ptr *int64, short string, long string, value int64, usage string) {
	global.Int64Var(ptr, short, long, value, usage)
}

func Uint(short string, long string, value uint, usage string) *uint {
	return global.Uint(short, long, value, usage)
}

func UintVar(ptr *uint, short string, long string, value uint, usage string) {
	global.UintVar(ptr, short, long, value, usage)
}

func Uint64(short string, long string, value uint64, usage string) *uint64 {
	return global.Uint64(short, long, value, usage)
}

func Uint64Var(ptr *uint64, short string, long string, value uint64, usage string) {
	global.Uint64Var(ptr, short, long, value, usage)
}

func Float(short string, long string, value float64, usage string) *float64 {
	return global.Float(short, long, value, usage)
}

func FloatVar(ptr *float64, short string, long string, value float64, usage string) {
	global.FloatVar(ptr, short, long, value, usage)
}

func String(short string, long string, value string, usage string) *string {
	return global.String(short, long, value, usage)
}

func StringVar(ptr *string, short string, long string, value string, usage string) {
	global.StringVar(ptr, short, long, value, usage)
}

func Size(short string, long string, value bytesize.Size, usage string) *bytesize.Size {
	return global.Size(short, long, value, usage)
}

func SizeVar(ptr *bytesize.Size, short string, long string, value bytesize.Size, usage string) {
	global.SizeVar(ptr, short, long, value, usage)
}

func Duration(short string, long string, value time.Duration, usage string) *time.Duration {
	return global.Duration(short, long, value, usage)
}

func DurationVar(ptr *time.Duration, short string, long string, value time.Duration, usage string) {
	global.DurationVar(ptr, short, long, value, usage)
}

func Bytes(short string, long string, value []byte, usage string) *[]byte {
	return global.Bytes(short, long, value, usage)
}

func BytesVar(ptr *[]byte, short string, long string, value []byte, usage string) {
	global.BytesVar(ptr, short, long, value, usage)
}

func IntSlice(short string, long string, value []int, usage string) *[]int {
	return global.IntSlice(short, long, value, usage)
}

func IntSliceVar(ptr *[]int, short string, long string, value []int, usage string) {
	global.IntSliceVar(ptr, short, long, value, usage)
}

func Int64Slice(short string, long string, value []int64, usage string) *[]int64 {
	return global.Int64Slice(short, long, value, usage)
}

func Int64SliceVar(ptr *[]int64, short string, long string, value []int64, usage string) {
	global.Int64SliceVar(ptr, short, long, value, usage)
}

func UintSlice(short string, long string, value []uint, usage string) *[]uint {
	return global.UintSlice(short, long, value, usage)
}

func UintSliceVar(ptr *[]uint, short string, long string, value []uint, usage string) {
	global.UintSliceVar(ptr, short, long, value, usage)
}

func Uint64Slice(short string, long string, value []uint64, usage string) *[]uint64 {
	return global.Uint64Slice(short, long, value, usage)
}

func Uint64SliceVar(ptr *[]uint64, short string, long string, value []uint64, usage string) {
	global.Uint64SliceVar(ptr, short, long, value, usage)
}

func FloatSlice(short string, long string, value []float64, usage string) *[]float64 {
	return global.FloatSlice(short, long, value, usage)
}

func FloatSliceVar(ptr *[]float64, short string, long string, value []float64, usage string) {
	global.FloatSliceVar(ptr, short, long, value, usage)
}

func StringSlice(short string, long string, value []string, usage string) *[]string {
	return global.StringSlice(short, long, value, usage)
}

func StringSliceVar(ptr *[]string, short string, long string, value []string, usage string) {
	global.StringSliceVar(ptr, short, long, value, usage)
}

func StructFields(dst interface{}) {
	global.StructFields(dst)
}

func Var(ptr Value, short string, long string, value string, usage string) {
	global.Var(ptr, short, long, value, usage)
}

func Add(flag Flag) {
	global.Add(flag)
}

func Parse() ([]string, error) {
	return global.Parse(os.Args[1:])
}

func MustParse() []string {
	unknown, err := Parse()
	if err != nil {
		panic(err)
	}
	return unknown
}
