package fflag

import (
	"strings"
)

type Flag struct {
	// Short is the short-form flag name, parsed from CLI flags prefixed by '-'.
	Short string

	// Long is the long-form flag name, parsed from CLI flags prefixed by '--'.
	Long string

	// Usage is the description for this CLI flag, used when generating usage string.
	Usage string

	// Required specifies whether this flag is required, if so returning error.
	Required bool

	// Default is the default value for this CLI flag, used when no value provided.
	Default string

	// RawValues contains raw value strings populated during parse stage.
	RawValues []string

	// Value stores the actual value pointer that is set upon argument parsing.
	Value Value
}

// Name returns a CLI readable name for this flag, preferring long. e.g. "--long".
func (f Flag) Name() string {
	switch {
	case f.Long != "":
		return "--" + f.Long
	case f.Short != "":
		return "-" + f.Short
	default:
		return ""
	}
}

// AppendUsage will append (new-line terminated) usage information for flag to b.
func (f Flag) AppendUsage(b []byte) []byte {
	if f.Short != "" {
		// Append short-flag
		b = append(b, " -"...)
		b = append(b, f.Short...)
	}

	if f.Long != "" {
		// Append long-flag
		b = append(b, " --"...)
		b = append(b, f.Long...)
	}

	if kind := f.Value.Kind(); kind != "" {
		// Append type information
		b = append(b, ' ')
		b = append(b, kind...)
	}

	if f.Usage != "" {
		// Append usage information
		b = append(b, "\n    \t"...)
		b = append(b, strings.ReplaceAll(
			// Replace new-lines with
			// prefixed new-lines.
			f.Usage,
			"\n",
			"\n    \t",
		)...)
	}

	if f.Default != "" {
		// Append default information
		b = append(b, " (default "...)
		b = append(b, f.Default...)
		b = append(b, ')')
	}

	b = append(b, '\n')
	return b
}
