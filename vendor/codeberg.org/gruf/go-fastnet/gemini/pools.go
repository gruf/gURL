package gemini

import (
	"sync"

	"codeberg.org/gruf/go-pools"
)

// rbufPool is the global reader buffer pool
var rbufPool = pools.NewBufioReaderPool(4096)

// wbufPool is the global writer buffer pool
var wbufPool = pools.NewBufioWriterPool(4096)

// mimeBuf is a simple 512 byte array for
// parsing the first 512 bytes of a file for mime type
type mimeBuf [512]byte

// mimeOnce ensures the mimeBufPool is only allocated once
var mimeOnce = sync.Once{}

// mimeBufPool is the global mimeBuf allocation pool
var mimeBufPool *sync.Pool

// initMimePool ensures mimeBufPool is prepared
func initMimePool() {
	mimeOnce.Do(func() {
		mimeBufPool = &sync.Pool{
			New: func() interface{} {
				return &mimeBuf{}
			},
		}
	})
}
