package gemini

import (
	"io"
	"net/http"
	"os"
	"path"

	"codeberg.org/gruf/go-fastnet"
	"codeberg.org/gruf/go-mimetypes"
)

// gemtext mime type string
var gemtextMime = "text/gemini"

// MimeType returns the parsed mime-type for a file path
func MimeType(filename string) (string, bool) {
	// Get path extension
	ext := path.Ext(filename)
	if len(ext) < 1 {
		return "", false
	}

	// Skip past the '.'
	ext = ext[1:]

	// Look for gemini file-types
	if ext == "gmi" || ext == "gemini" {
		return gemtextMime, true
	}

	// Look for all other mimetypes
	return mimetypes.GetForExt(ext)
}

// ServeFile attempts to parse a mime-type from either the file's extension, or first 512-bytes
// of the file, to set an appropriate response header. Before finally serving content to the client
func ServeFile(ctx *fastnet.RequestCtx, rw ResponseWriter, r *Request, file *os.File) {
	// Attempt to get mimeType from name
	mimeType, ok := MimeType(file.Name())
	if ok {
		// Write mime header
		rw.WriteHeader(StatusSuccess, mimeType)

		// Copy file to rsp
		rw.ReadFrom(file)
	} else {
		// ensure pool running
		initMimePool()

		// Get 512 byte mime detect buffer
		buf := mimeBufPool.Get().(*mimeBuf)
		defer mimeBufPool.Put(buf)

		// Attempt to read file into buf
		n, err := io.ReadFull(file, buf[:])
		if err != nil && err != io.ErrUnexpectedEOF {
			rw.WriteHeader(StatusNotFound, StatusText(StatusNotFound))
			return
		}

		// Detect mime type from bytes + write header
		mimeType = http.DetectContentType(buf[:n])
		rw.WriteHeader(StatusSuccess, mimeType)

		// Write first however-many bytes
		rw.Write(buf[:n])

		// If there were more bytes, write remaining
		if n >= len(buf) {
			rw.ReadFrom(file)
		}
	}
}
