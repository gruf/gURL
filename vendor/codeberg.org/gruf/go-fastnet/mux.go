package fastnet

import "codeberg.org/gruf/go-bytes"

type Mux struct {
	noCopy noCopy //nolint:unused,structcheck

	// RootHandler is the default handler used when no path handler is found
	RootHandler Handler

	// PanicHandler is used to catch any panics during other handler execution
	PanicHandler func(*RequestCtx, interface{})

	// root is the root node of the radix tree
	root node
}

// Handle adds the supplied handler to the mux under the supplied path
func (m *Mux) Handle(path string, handler Handler) {
	// If NotFoundHandler unset, use default
	if m.RootHandler == nil {
		m.RootHandler = HandlerFunc(func(*RequestCtx) {})
	}

	// Add route via root tree node
	m.root.add(bytes.StringToBytes(path), handler)
}

// Serve attempts to serve the supplied RequestCtx using a set Handler, else uses the RootHandler
func (m *Mux) Serve(ctx *RequestCtx) {
	// Look for handler in tree
	handler, ok := m.root.get(ctx)

	// Defer panic handler
	defer func() {
		r := recover()
		if r != nil {
			m.PanicHandler(ctx, r)
		}
	}()

	if !ok {
		// Handler not found, pass to root...
		m.RootHandler.Serve(ctx)
		return
	}

	// Use found handler
	handler.Serve(ctx)
}
