opinionated TCP server library -- i.e. you can use this to help write your [insert TCP-based protocol] server library

currently provides a Gemini(+Atlas) server implementation

thanks to the libraries:

- [go-uri](https://codeberg.org/gruf/go-uri) for URI parsing

- [fasthttp](https://github.com/valyala/fasthttp) which much of the `RequestCtx` API is based upon,
and for providing `SO_REUSEPORT` support for tcp listeners

- [httprouter](https://github.com/julienschmidt/httprouter) for the awesome
radix tree routing implementation that this library's mux is based upon