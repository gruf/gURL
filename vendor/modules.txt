# codeberg.org/gruf/go-bytes v1.0.2
## explicit; go 1.14
codeberg.org/gruf/go-bytes
# codeberg.org/gruf/go-bytesize v1.0.2
## explicit; go 1.17
codeberg.org/gruf/go-bytesize
# codeberg.org/gruf/go-byteutil v1.1.2
## explicit; go 1.16
codeberg.org/gruf/go-byteutil
# codeberg.org/gruf/go-ctx v1.0.2
## explicit; go 1.17
codeberg.org/gruf/go-ctx
# codeberg.org/gruf/go-errors v1.0.5
## explicit; go 1.15
codeberg.org/gruf/go-errors
# codeberg.org/gruf/go-fastnet v1.3.1
## explicit; go 1.16
codeberg.org/gruf/go-fastnet
codeberg.org/gruf/go-fastnet/gemini
# codeberg.org/gruf/go-fastpath v1.0.2
## explicit; go 1.14
codeberg.org/gruf/go-fastpath
# codeberg.org/gruf/go-fflag v0.0.0-20230414222726-79db39efa8eb
## explicit; go 1.19
codeberg.org/gruf/go-fflag
# codeberg.org/gruf/go-format v1.0.3
## explicit; go 1.17
codeberg.org/gruf/go-format
# codeberg.org/gruf/go-fsys v0.0.0-20230508183124-d1809af32b35
## explicit; go 1.19
codeberg.org/gruf/go-fsys
# codeberg.org/gruf/go-kv v1.6.1
## explicit; go 1.19
codeberg.org/gruf/go-kv/format
# codeberg.org/gruf/go-mimetypes v1.0.0
## explicit; go 1.17
codeberg.org/gruf/go-mimetypes
# codeberg.org/gruf/go-pools v1.0.2
## explicit; go 1.16
codeberg.org/gruf/go-pools
# codeberg.org/gruf/go-split v1.1.0
## explicit; go 1.19
codeberg.org/gruf/go-split
# codeberg.org/gruf/go-uri v1.0.5
## explicit; go 1.16
codeberg.org/gruf/go-uri
# golang.org/x/exp v0.0.0-20221031165847-c99f073a8326
## explicit; go 1.18
golang.org/x/exp/constraints
golang.org/x/exp/slices
# golang.org/x/sys v0.8.0
## explicit; go 1.17
golang.org/x/sys/unix
