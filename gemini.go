package main

import (
	"bufio"
	"context"
	"crypto/tls"
	"io"
	"net"
	"strconv"

	"codeberg.org/gruf/go-bytes"
	"codeberg.org/gruf/go-fastnet/gemini"
	"codeberg.org/gruf/go-uri"
)

func GeminiHandler(ctx context.Context, dialer *tls.Dialer, uri *uri.URI, redirect, max int) error {
	// Get host with port (and update URI obj)
	host := uri.Host().StringPtr()
	_, _, err := net.SplitHostPort(host)
	if err != nil {
		// Set the default port since none provided
		uri.SetHostString(host + ":1965")
		host = uri.Host().StringPtr()
	}

	// Dial TCP connection to supplied host addr
	vprintf("* connecting to host: {} ...", host)
	conn, err := dialer.DialContext(ctx, "tcp", host)
	if err != nil {
		return errorf("error connecting to host: {}", err)
	}
	defer conn.Close()
	vprintln("* connected!")

	// Prepare request bytes (terminate with CRLF)
	fullURI := uri.FullURI()
	req := append(fullURI.Bytes(), '\r', '\n')
	vprintf("> {}", fullURI.StringPtr())

	// Write request bytes
	_, err = conn.Write(req)
	if err != nil {
		return errorf("error writing to host: {}", err)
	}

	// Wrap connection in read buffer
	rbuf := bufio.NewReader(conn)

	// Read first line of response
	line, err := rbuf.ReadSlice('\n')
	if err != nil && err != io.EOF {
		return errorf("error reading response header: {}", err)
	} else if len(line) < 4 /* minimum hdr size */ {
		return errorf("invalid response header from server: {}", line)
	}

	// Strip CRLF header closer
	line = bytes.TrimBytePrefix(line[:len(line)-2], '\r')
	vprintf("< {}", line)

	// Parse gemini header from response
	code, meta, ok := ParseGeminiHeader(line)
	if !ok {
		return errorf("invalid response header from server: {}", line)
	}

	switch code {
	// All good, nothing to do :)
	case gemini.StatusSuccess:
		break

	// Server redirects, recurse into them
	case gemini.StatusTemporaryRedirect:
		fallthrough
	case gemini.StatusPermanentRedirect:
		if redirect < max {
			// Parse redirect URL from header
			if err := uri.Parse(meta); err != nil {
				return errorf("error parsing redirect meta {}: {}", meta, err)
			} else if uri.Scheme().StringPtr() != "gemini" {
				return errorf("unexpected redirect protocol: {}", uri.FullURI().StringPtr())
			}

			// Recurse into handler with new redirect URL
			vprintf("* following '{}' redirect: {}", code, meta)
			return GeminiHandler(ctx, dialer, uri, redirect+1, max)
		}

		// Treat as success
		break

	// Server error
	default:
		return errorf("server error response: {}", line)
	}

	// Write response from server to stdout
	_, err = rbuf.WriteTo(stdout)
	if err != nil {
		return errorf("error reading from host: {}", err)
	}

	return nil
}

// ParseGeminiHeader parses a gemini header from supplied line
func ParseGeminiHeader(b []byte) (int, []byte, bool) {
	split := bytes.SplitN(b, []byte{' '}, 2)
	if len(split) != 2 {
		return 0, nil, false
	}
	code, err := strconv.ParseInt(
		bytes.BytesToString(split[0]), 10, 8,
	)
	if err != nil {
		return 0, nil, false
	}
	return int(code), split[1], true
}
