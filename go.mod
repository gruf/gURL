module codeberg.org/gruf/gurl

go 1.17

require (
	codeberg.org/gruf/go-bytes v1.0.2
	codeberg.org/gruf/go-byteutil v1.1.2
	codeberg.org/gruf/go-fastnet v1.3.1
	codeberg.org/gruf/go-fflag v0.0.0-20230414222726-79db39efa8eb
	codeberg.org/gruf/go-fsys v0.0.0-20230508183124-d1809af32b35
	codeberg.org/gruf/go-kv v1.6.1
	codeberg.org/gruf/go-uri v1.0.5
	golang.org/x/sys v0.8.0
)

require (
	codeberg.org/gruf/go-bytesize v1.0.2 // indirect
	codeberg.org/gruf/go-ctx v1.0.2 // indirect
	codeberg.org/gruf/go-errors v1.0.5 // indirect
	codeberg.org/gruf/go-fastpath v1.0.2 // indirect
	codeberg.org/gruf/go-format v1.0.3 // indirect
	codeberg.org/gruf/go-mimetypes v1.0.0 // indirect
	codeberg.org/gruf/go-pools v1.0.2 // indirect
	codeberg.org/gruf/go-split v1.1.0 // indirect
	golang.org/x/exp v0.0.0-20221031165847-c99f073a8326 // indirect
)
