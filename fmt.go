package main

import (
	"errors"

	"codeberg.org/gruf/go-kv/format"
    "codeberg.org/gruf/go-byteutil"
)

var (
	// print format buffer (not concurrency safe).
	fbuf = byteutil.Buffer{B: make([]byte, 0, 512)}

	// verbose print mode.
	verbose bool
)

// errorf creates a new error from formatted string.
func errorf(s string, a ...interface{}) error {
	fbuf.Reset() // reset buffer
	format.Appendf(&fbuf, s, a...)
	return errors.New(string(fbuf.B))
}

// printf will print formatted string to stdout.
func printf(s string, a ...interface{}) {
	fbuf.Reset() // reset buffer
	format.Appendf(&fbuf, s, a...)
	fbuf.B = append(fbuf.B, '\n')
	_, _ = stdout.Write(fbuf.B)
}

// eprintf will print formatted string to stderr.
func eprintf(s string, a ...interface{}) {
	fbuf.Reset() // reset buffer
	format.Appendf(&fbuf, s, a...)
	fbuf.B = append(fbuf.B, '\n')
	_, _ = stderr.Write(fbuf.B)
}

// vprintln will print string followed by new-line to stderr only if verbose enabled.
func vprintln(s string) {
	if verbose {
		_, _ = stderr.WriteString(s + "\n")
	}
}

// vprintf will print formatted string to stderr only if verbose enabled.
func vprintf(s string, a ...interface{}) {
	if verbose {
		eprintf(s, a...)
	}
}
