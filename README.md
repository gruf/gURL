# gURL

Gemini command-line client similar to cURL.

build from main

todo:
- handle gemini TOFU
- more customizable flags (buf sizes, etc)
- documentation
- consistent (and documented) exit codes