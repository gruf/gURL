package main

import (
	"io"

	"codeberg.org/gruf/go-fsys"
)

var (
	// std file descriptors.
	stdin  file = fsys.Stdin()
	stdout file = fsys.Stdout()
	stderr file = fsys.Stderr()
)

// file represents a base unix file interface.
type file interface {
	io.Reader
	io.Writer
	io.StringWriter
	io.Closer
}

// devnull is a file{} implementation
// that reads and writes to the void (no-op).
type devnull struct{}

func (*devnull) Read(b []byte) (int, error) { return len(b), nil }

func (*devnull) Write(b []byte) (int, error) { return len(b), nil }

func (*devnull) WriteString(s string) (int, error) { return len(s), nil }

func (*devnull) Close() error { return nil }
