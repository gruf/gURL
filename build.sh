#!/bin/sh

set -e

# Get git commit hash
COMMIT=$(git rev-parse --short HEAD)

# Determine version str
VERSION=$(git name-rev --name-only ${COMMIT})
VERSION=${VERSION#tags/}

GOAMD64=v4 \
CGO_ENABLED=0 \
go build -trimpath -v \
         -tags 'netgo osusergo static_build kvformat' \
         -ldflags "-s -w" \
         -gcflags=all='-l=4 -B -C' \
         -o 'gurl' \
         ./*.go
