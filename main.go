package main

import (
	"context"
	"crypto/tls"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"codeberg.org/gruf/go-fflag"
	"codeberg.org/gruf/go-fsys"
	"codeberg.org/gruf/go-uri"
	"golang.org/x/sys/unix"
)

func main() {
	var code int

	// Run main application
	if err := run(); err != nil {
		eprintf("FATAL: {}", err)
		code = 1
	}

	// Exit with code
	unix.Exit(code)
}

func run() error {
	var (
		redirect int
		tlsCert  string
		tlsKey   string
		tlsInsec bool
		output   string
		timeout  time.Duration
	)

	// Declare runtime flags to global fflag.FlagSet
	fflag.BoolVar(&tlsInsec, "i", "tls-insecure", false, "Skip TLS cert verification.")
	fflag.IntVar(&redirect, "r", "redirect-count", 0, "Max number redirects to follow.")
	fflag.StringVar(&tlsCert, "c", "tls-cert", "", "TLS client certificate file.")
	fflag.StringVar(&tlsKey, "k", "tls-key", "", "TLS client private key file.")
	fflag.StringVar(&output, "o", "output", "", "Set output file path. (default: stdout)")
	fflag.DurationVar(&timeout, "t", "timeout", time.Minute, "Set request timeout period.")
	fflag.BoolVar(&verbose, "V", "verbose", false, "Enable verbose mode.")
	fflag.Func("h", "help", "Print usage information.", func() {
		stdout.WriteString("Usage: gurl <URI> ...\n" + fflag.Usage())
		unix.Exit(0)
	})

	// Parse runtime flags.
	args, err := fflag.Parse()
	if err != nil {
		return err
	}

	// Ensure URI given.
	if len(args) != 1 {
		stdout.WriteString("Usage: gurl <URL> ...\n" + fflag.Usage())
		unix.Exit(1)
	}

	if output != "" {
		// attempt to open given output file as replacement stdout.
		stdout, _, err = fsys.OpenFile(output, unix.O_CREAT, 0o644)
		if err != nil {
			return errorf("error opening output '{}': {}", output, err)
		}

		// close file on exit.
		defer stdout.Close()
	}

	// Allocate request URI.
	uri := uri.New()

	// Add a default URI schema of gemini.
	if !strings.HasPrefix(args[0], "://") {
		args[0] = "gemini://" + args[0]
	}

	// Parse provided raw URL string
	err = uri.ParseString(args[0])
	if err != nil {
		return errorf("error parsing uri: {}", err)
	}

	// Check that we have a supported protocol (only gemini).
	if proto := uri.Scheme().StringPtr(); proto != "gemini" {
		return errorf("unsupported protocol: {}", proto)
	}

	var certs []tls.Certificate

	if tlsCert != "" && tlsKey != "" {
		// Load provided client certifcate key-pair.
		cert, err := tls.LoadX509KeyPair(tlsCert, tlsKey)
		if err != nil {
			return errorf("error loading certificate: {}", err)
		}

		// Set client certificate.
		certs = []tls.Certificate{cert}
	}

	// Create TLS net dialer.
	tlsDialer := tls.Dialer{
		Config: &tls.Config{
			Certificates:       certs,
			InsecureSkipVerify: tlsInsec,
		},
	}

	// Cancel base ctx on OS signal.
	ctx, _ := signal.NotifyContext(
		context.Background(),
		syscall.SIGINT,
		syscall.SIGTERM,
	)

	// Wrap the context to timeout after given time.
	ctx, cncl := context.WithTimeout(ctx, timeout)
	defer cncl()

	// Pass to main gemini handler.
	return GeminiHandler(ctx,
		&tlsDialer,
		&uri,
		0,
		redirect,
	)
}
